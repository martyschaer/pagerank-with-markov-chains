---
title:          Pagerank mit Markov-Chains
author:
  - Severin Kaderli
  - Marius Schär
date:           2018-06-08
lang:           de-CH
links-as notes: true
classoption:
  - aspectratio=169
mainfont:       Helvetica Neue
mathfont:       Latin Modern Math

---


## Material

\LARGE{markov.mariusschaer.ch} ^[Link](https://markov.mariusschaer.ch)^

# Markov Chains

## Definition
"a stochastic model describing a sequence of possible events in which the probability of each event depends only on the state attained in the previous event"

## Interaktiver Playground

\Large{setosa.io/ev/markov-chains} ^[Link](http://setosa.io/ev/markov-chains/)^

# Beispiel
## Seiten
\colsbegin
\column{.5\textwidth}

\tikzset{
  LabelStyle/.style = { rectangle, rounded corners, draw,
                        minimum width = 2em,text = black, scale=0.75},
  VertexStyle/.append style = { inner sep=5pt, outer sep=5pt},
  EdgeStyle/.append style = {->, bend left} }

\begin{tikzpicture}
  \SetGraphUnit{2}
  \Vertex{B}
  \WE(B){A}
  \EA(B){C}
  \Edge[label = 0.5](A)(B)
  \Edge[label = 1](B)(C)
  \Edge[label = 1](C)(A)
  \tikzset{EdgeStyle/.append style = {bend left = 50}}
  \Edge[label = 0.5](A)(C)
\end{tikzpicture}

\column{.5\textwidth}

Graph unseres PageRank-Raumes.

\colsend

## Iterative PageRank Formel
  $$
  PR(X) = (1-d) + d (\frac{PR(T_1)}{C(T_1)} + ... + \frac{PR(T_n)}{C(T_n)})
  $$

## Resultate

Iteration| $PR(A)$| $PR(B)$| $PR(C)$
:-------:|:----:|:----:|:----:
0|1|1|1
1|1|0.75|1.125
2|1.0625|0.7656|1.1484
$\vdots$|$\vdots$|$\vdots$|$\vdots$|
12|1.0769|0.7692|1.1538

## Übergangsmatrix
\colsbegin
\column{.5\textwidth}

\tikzset{
  LabelStyle/.style = { rectangle, rounded corners, draw,
                        minimum width = 2em,text = black, scale=0.75},
  VertexStyle/.append style = { inner sep=5pt, outer sep=5pt},
  EdgeStyle/.append style = {->, bend left} }

\begin{tikzpicture}
  \SetGraphUnit{2}
  \Vertex{B}
  \WE(B){A}
  \EA(B){C}
  \Edge[label = 0.5](A)(B)
  \Edge[label = 1](B)(C)
  \Edge[label = 1](C)(A)
  \tikzset{EdgeStyle/.append style = {bend left = 50}}
  \Edge[label = 0.5](A)(C)
\end{tikzpicture}

\column{.5\textwidth}

$$
\text{Übergangsmatrix } P = \bordermatrix{  ~ & A   & B & C \cr
                A & 0 & 0.5 & 0.5 \cr
                B & 0 & 0   & 1   \cr
                C & 1 & 0   & 0   \cr}
$$

\colsend

## Google Matrix

$$
\text{Google Matrix }M = d \cdot P + (1 - d) \cdot R
$$

\center{Jedes Element der Matrix $R$ ist $\frac{1}{n}$.}

## Google Matrix (Dämpfungsfaktor 0.85)
$$
M = \begin{vmatrix}
0 & 0.425 & 0.425 \\
0 & 0 & 0.85 \\
0.85& 0 & 0
\end{vmatrix}
+
\begin{vmatrix}
0.0495 & 0.0495 & 0.0495 \\
0.0495 & 0.0495 & 0.0495 \\
0.0495 &0.0495 & 0.0495
\end{vmatrix}
=
\begin{vmatrix}
0.0495 & 0.4745 & 0.4745 \\
0.0495 & 0.0495 & 0.8995 \\
0.8995 & 0.0495 & 0.0495
\end{vmatrix}
$$

## Verwendung der Google Matrix
$$
x_{0} = \begin{pmatrix}1 & 1 & 1\end{pmatrix}
$$

$$
\Rightarrow x_{0} \cdot \text{ Google Matrix}
$$

## Verwendung der Google Matrix cont.
| Iteration | $PR(A)$  | $PR(B)$  | $PR(C)$  |
| :-------: | :------: | :------: | :------: |
|     0     |    1     |    1     |    1     |
|     1     |  0.099   |  0.574   |  1.424   |
|     2     |  1.314   |  0.146   |  0.634   |
|     3     |  0.643   |  0.662   |  0.785   |
| $\vdots$  | $\vdots$ | $\vdots$ | $\vdots$ |
|    30     |  1.112   |  0.611   |  1.140   |

$$
x_{30} = \begin{pmatrix}1.112 & 0.611 & 1.140\end{pmatrix} = \text{ Stationäre Verteilung}
$$

## Resultat
\colsbegin
\column{.5\textwidth}

\tikzset{
  LabelStyle/.style = { rectangle, rounded corners, draw,
                        minimum width = 2em,text = black, scale=0.75},
  VertexStyle/.append style = { inner sep=5pt, outer sep=5pt},
  EdgeStyle/.append style = {->, bend left} }

\begin{tikzpicture}
  \SetGraphUnit{2}
  \Vertex{B}
  \WE(B){A}
  \EA(B){C}
  \Edge[label = 0.5](A)(B)
  \Edge[label = 1](B)(C)
  \Edge[label = 1](C)(A)
  \tikzset{EdgeStyle/.append style = {bend left = 50}}
  \Edge[label = 0.5](A)(C)
\end{tikzpicture}

\column{.5\textwidth}

$$
\text{PageRank }= \bordermatrix{  ~ & A   & B & C \cr
               ~ & 1.112 & 0.611 & 1.140\cr}
$$

\colsend

## Fragen?
\center{\LARGE{Fragen?}}

## Danke

\begin{center}
\LARGE{Danke}

\Large{markov.mariusschaer.ch}
\end{center}
