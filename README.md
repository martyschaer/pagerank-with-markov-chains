# PageRank mit Markov-Ketten

Präsentation und Zusammenfassung über das Thema PageRank-Algorithmus mithilfe von Markov-Ketten. Dies war eine Projektarbeit für das Modul BZG1152-LINALG an der BFH.

## Autoren

| Name            | Website                                        |
| --------------- | ---------------------------------------------- |
| Severin Kaderli | [severinkaderli.ch](https://severinkaderli.ch) |
| Marius Schär    | [mariusschaer.ch](https://mariusschaer.ch)     |

## Unterlagen

Die Präsentationsfolien und Zusammenfassung im PDF-Format können als Build-Artefakte [hier gefunden](https://gitlab.com/martyschaer/pagerank-with-markov-chains/builds/artifacts/master/browse?job=PDF) werden.

## Kompilierung

Die Präsentation und Zusammenfassung können mithilfe von Pandoc aus den vorhandenen Markdown-Dokumenten generiert werden. Dafür steht ein makefile zur Verfügung.

Die Abhängigkeiten für die Kompilierung ist unter [DEPENDS](https://gitlab.com/martyschaer/pagerank-with-markov-chains/blob/master/DEPENDS) festgehalten.

## Code-Beispiele

Eine Beispiel Implementation in Java für den PageRank-Algorithmus ist in folgendem Repository zu finden: [https://gitlab.com/severinkaderli/Java-PageRank](https://gitlab.com/severinkaderli/Java-PageRank)

## Material / Quellen

Die Dateien im Ordner [material](https://gitlab.com/martyschaer/pagerank-with-markov-chains/tree/master/material) wurden als Quelle für die Recherche dieser Präsentation und der Zusammenfassung verwendet. Sie wurden nicht von uns hergestellt.
