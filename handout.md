---
title:          Pagerank
author:
  - Severin Kaderli
  - Marius Schär
toc:            true
toc-depth:      3
papersize:      a4
lang:           de-CH
links-as-notes:
  - true
geometry:
  - margin=2.5cm
---

[comment]: # "This is to achieve a pagebreak after the TOC"

\newpage

# Markov-Ketten

Markov-Ketten werden dazu verwendet um Situationen zu modellieren die aus verschiedenen Zuständen bestehen, wobei für die verschiedenen Übergänge zwischen den Zuständen jeweils verschiedene Wahrscheinlichkeiten gelten.

Dabei ist jeweils nur der aktuelle Zustand entscheidend für die Findung des nächsten Zustandes und wird nicht durch vorherige Zustände beeinflusst. Deshalb werden Markov-Ketten auch als gedächtnislos bezeichnet.

Diese Wahrscheinlichkeiten kann für die einfache Übersichtlichkeit als sogenannte Übergangsmatrix (engl. transition matrix) darstellen. Dabei sollten die Zeilensummen jeweils auf 1 kommen, da es sich dabei um eine sogenannte *zeilenstochastische Matrix* handelt.

Die folgende Übergangsmatrix  zeigt die Wahrscheinlichkeit der Wetterveränderungen. Wenn heute die Sonne scheint gibt es eine 90% Wahrscheinlichkeit, dass am nächsten Tag auch die Sonne scheint und eine 10% Wahrscheinlichkeit, dass es regnet. Falls es heute regnet, ist die Wahrscheinlichkeit, dass es am nächsten Tag regnet oder die Sonne scheint genau gleich.
$$
P = \begin{vmatrix}
0.9 & 0.1 \\
0.5 & 0.5
\end{vmatrix}
$$

\begin{center}
\tikzset{
  LabelStyle/.style = { rectangle, rounded corners, draw, fill = white,
                        minimum width = 2em,text = black, scale=0.75},
  VertexStyle/.append style = { inner sep=5pt, outer sep=5pt},
  EdgeStyle/.append style = {->, bend left} }

\begin{tikzpicture}
  \SetGraphUnit{3}
  \Vertex{R}
  \WE(R){S}
  \Edge[label = 0.1](S)(R)
  \Loop[dist = 3cm,dir = NO,label = 0.9](S.west)
  \Edge[label = 0.5](R)(S)
  \Loop[dist = 3cm,dir = SO,label = 0.5](R.east)
\end{tikzpicture}
\end{center}

Man weiss, dass heute die Sonne scheint. Also definiert man eine Anfangsverteilung als Vektor, wo die Sonne zu 100% scheint.
$$
x_{0} = \begin{pmatrix}1 & 0\end{pmatrix}
$$
Das Wetter am nächsten Tag kann man vorraussagen, indem man die Anfangsverteilung mit der Übergangsmatrix multipliziert.
$$
x_{1} = x_{0} \cdot P =
\begin{pmatrix}1 & 0\end{pmatrix}
\cdot 
\begin{vmatrix}
0.9 & 0.1 \\
0.5 & 0.5
\end{vmatrix}
=
\begin{pmatrix}0.9 & 0.1\end{pmatrix}
$$
Das Wetter am nächsten Tag kann man nun auf zwei Weisen berechnen.
$$
x_{2} = x_{0} * P^{2} =
\begin{pmatrix}1 & 0\end{pmatrix}
\cdot 
\begin{vmatrix}
0.9 & 0.1 \\
0.5 & 0.5
\end{vmatrix} ^{2}
=
\begin{pmatrix}0.86 & 0.14\end{pmatrix}
$$

$$
x_{2} = x_{1} * P =
\begin{pmatrix}0.9 & 0.1\end{pmatrix}
\cdot 
\begin{vmatrix}
0.9 & 0.1 \\
0.5 & 0.5
\end{vmatrix}
=
\begin{pmatrix}0.86 & 0.14\end{pmatrix}
$$

Daraus folgt, dass man das Wetter eines beliebigen Tages $n$ nur anhand der Anfangsverteilung auf folgende Weisen berechnen kann.
$$
x_{n} = x_{0} * P^{n}
$$

$$
x_{n} = x_{n - 1} * P
$$

\newpage

# PageRank

Der PageRank-Algorithmus ist ein Verfahren um verlinkte Dokumente zu bewerten und zu gewichten. Der Algorithmus wurde von Larry Page und Sergei Brin entwickelt und diente für die Suchmaschine Google als Grundlage für die Seitenbewertung. Diese Gewichtung ist der sogenannte PageRank.

Je mehr Links dabei auf eine Seite verweisen, desto höher ist das Gewicht dieser Seite und desto höher das Gewicht der verweisenden Seiten ist, desto höher ist der Effekt davon.

Als Beispiel für den PageRank benutzen wir die folgende Seitenstruktur mit den drei Seiten  $A$, $B$ und $C$ die untereinander verlinkt sind.

\begin{center}
\tikzset{
  LabelStyle/.style = { rectangle, rounded corners, draw,
                        minimum width = 2em,text = black, scale=0.75},
  VertexStyle/.append style = { inner sep=5pt, outer sep=5pt},
  EdgeStyle/.append style = {->, bend left} }

\begin{tikzpicture}
  \SetGraphUnit{2}
  \Vertex{B}
  \WE(B){A}
  \EA(B){C}
  \Edge[label = 0.5](A)(B)
  \Edge[label = 1](B)(C)
  \Edge[label = 1](C)(A)
  \tikzset{EdgeStyle/.append style = {bend left = 50}}
  \Edge[label = 0.5](A)(C)
\end{tikzpicture}
\end{center}

Wenn man nun den PageRank von einer Seite erfahren möchte kann man die folgende Formel verwenden. Dabei ist $PR(X)$ der PageRank der Seite $X$, $d$ ist der sogenannte Dämpungsfaktor, welcher bei Google als $0.85$ festgelegt ist. $C(X)$ sind die Anzahl Links die von der Seite $X$ herausgehen. Schlussendlich die Menge $\{T_{1}, T_{2}, \dots, T_{n}\}$ sind die Seiten die zur Seite $X$ verlinken.
$$
PR(X) = (1-d) + d (\frac{PR(T_1)}{C(T_1)} + ... + \frac{PR(T_n)}{C(T_n)})
$$
Da man zu Beginn noch keinen PageRank der Seiten weiss und diesen für die Rechnung benötigt kann man als Standard PageRank den Wert $1$ nehmen.

Wenn man diese Formel immer wieder auf die Seiten anwendet, nähert sich der PageRank der Seiten nach jeder Iteration seinem eigentlichen Wert.

| Iteration | $PR(A)$  | $PR(B)$  | $PR(C)$  |
| :-------: | :------: | :------: | :------: |
|     0     |    1     |    1     |    1     |
|     1     |    1     |   0.75   |  1.125   |
|     2     |  1.0625  |  0.7656  |  1.1484  |
| $\vdots$  | $\vdots$ | $\vdots$ | $\vdots$ |
|    12     |  1.0769  |  0.7692  |  1.1538  |
| $\vdots$  | $\vdots$ | $\vdots$ | $\vdots$ |



Nun kann man diese Berechnung für den PageRank noch auf eine andere Weise durchführen und zwar mit der Hilfe von Markov-Ketten. Man kann diese Wahrscheinlichkeiten um von einer Seite auf eine andere zu gelangen in einer Übergangsmatrix darstellen. Dies sieht bei unserem Beispiel folgendermassen aus.
$$
P = \begin{vmatrix}
0 & 0.5 & 0.5 \\
0 & 0   & 1 \\
1 & 0   & 0 
\end{vmatrix}
$$
Laut dem PageRank-Algorithmus wird jetzt aus der gegebenen Übergangsmatrix die sogennante Google Matrix erstellt. Diese wird nach der folgenden Formel erstellt.
$$
M = d * P + (1 - d) * R
$$
$R$ ist dabei eine Matrix bei der jedes Element $\frac{1}{n}$ ist.

Mit dieser Berechnung mit der Annahme, dass wir $d = 0.85$ verwenden kommen wir auf die folgende Google Matrix $M$.
$$
M = \begin{vmatrix}
0 &	0.425 & 0.425 \\
0 & 0 & 0.85 \\
0.85& 0 & 0
\end{vmatrix}
+
\begin{vmatrix}
0.0495 & 0.0495 & 0.0495 \\
0.0495 & 0.0495 & 0.0495 \\
0.0495 &0.0495 & 0.0495
\end{vmatrix}
=
\begin{vmatrix}
0.0495 & 0.4745 & 0.4745 \\
0.0495 & 0.0495 & 0.8995 \\
0.8995 & 0.0495 & 0.0495
\end{vmatrix}
$$


Nun kann wie beim vorherigen Beispiel mit den Markov-Ketten vorgehen. Da wir nun eigentlich einen Web-Surfer simulieren der sich zufällig über Links auf neue Seiten begibt ist, die Anfangsverteilung gleichmässig verteilt.
$$
x_{0} = \begin{pmatrix}1 & 1 & 1\end{pmatrix}
$$
Nun können wir die Anfangsverteilung mit der Matrix multiplizieren und machen dies so oft bis sich der Vektor nicht mehr verändert und die sogenannte Stationäre Verteilung erreicht hat. Die Stationäre Verteilung wird, wenn man sie mit der Übergangsmatrix multipliziert wieder die Stationäre Verteilung ergeben. Sie ist also ein Eigenvektor der Übergangsmatrix zum Eigenwert $1$.

| Iteration | $PR(A)$  | $PR(B)$  | $PR(C)$  |
| :-------: | :------: | :------: | :------: |
|     0     |    1     |    1     |    1     |
|     1     |  0.099   |  0.574   |  1.424   |
|     2     |  1.314   |  0.146   |  0.634   |
|     3     |  0.643   |  0.662   |  0.785   |
| $\vdots$  | $\vdots$ | $\vdots$ | $\vdots$ |
|    30     |  1.112   |  0.611   |  1.140   |

